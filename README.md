# 28_News_Django

# E-News implemented by Group 28 using Django # 

1.Cấu trúc file
- News: Thư mục lớn nhất là project của mình, một project có thể chứa nhiều app.
- News/News: App chính, trong đây sẽ có các file models.py chứa các models chính, file setting để config cho app chính này,template để chưa các template html, view để quyết định những gì sẽ đổ ra template.
- News/Scrapy: App dùng để crawl data từ News.zing và Vnexpress và lưu vào database.
- Them cac file template vao thu muc News/News/template ke thua tu file base.html.


2.Set up để chạy 
- Tạo file config cùng cấp thư mục với file setting.py chưa các biến database, user, password,port đặt tên theo ý mình. 


3.Chạy app
- Cài đặt môi trường ảo trong thư mục 28_news_django
```bash 
python3 venv venv 
```
- Kích hoạt môi trường ảo cũng tại folder này.
```bash 
. venv/bin/activate 
```
- Cài đặt các package cần thiết django, scrapy,...
```bash
pip3 install tên-package
```

- Tạo model liên quan tới task của mình trong file models.py, mỗi lần thay đổi trong file models chạy câu lệnh:
```bash
python3 manage.py makemigrations
```
- Migrate vào database thật chạy câu lệnh:
```bash
python3 manage.py migrate

```
- Crawl data từ 2 trang web zing và vnexpress:
```bash 
cd crawl_news
```
crawl topic: 
```bash 
scrapy crawl topic 
```
crawl post 
```bash 
scrapy crawl article 
```
- Chay app, truy cap vao thu muc News/News
```bash 
cd News
```
- Mỗi lần chạy app chạy câu lệnh:
```bash
python3 manage.py runserver
```


