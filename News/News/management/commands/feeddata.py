from django.core.management.base import BaseCommand, CommandError

from News.models import Post,Topic,DetailedPost
import json
from datetime import datetime,time
from random import randint
from django.utils.timezone import now


class Command(BaseCommand):
    help = 'Migrate from json data'
    def add_arguments(self, parser):
        parser.add_argument('models', nargs='+', type= str)

    def handle(self, *args, **options):
        model = options['models'][0]
        if model == 'topic':
            with open('News/crawl_data/Topics.json') as json_file:
                topic_data = json.load(json_file)
                for topic in topic_data:
                    try:
                        tp = Topic(topic_id = topic['topic_id'],name_topic=topic['topic_name'],
                                   )
                        tp.save()
                    except Exception as e:
                        raise CommandError(f'Cannot migrate to db due to {e}')

            self.stdout.write(self.style.SUCCESS("Successfully migrated to topic models"))

        if model == 'post':
            with open('News/crawl_data/Article.json') as json_file:
                post_data = json.load(json_file)
                for post in post_data:
                    try:
                        topic_id = post['topic_id']
                        del post['topic_name']
                        if topic_id == -1:
                            continue
                        topic = Topic.objects.get(pk=topic_id)
                        date_string = post['date']
                        date = datetime.strptime(date_string,"%d/%m/%Y")
                        post['date'] = date
                        post['topic_id'] = topic
                        p = Post(**post)
                        p.save()
                    except Exception as e:
                        raise CommandError(f'Cannot migrate to db due to {e}')

            self.stdout.write(self.style.SUCCESS("Successfully migrated to post models"))

        if model == 'detail':
            with open('News/crawl_data/DetailPost.json') as json_file:
                detail_data = json.load(json_file)
                for detail in detail_data:
                    article_id = detail["article_id"]
                    try:
                        print(article_id)
                        article = Post.objects.get(article_id=article_id)
                    except Exception as e:
                        print(e)
                        continue
                    else:
                        date_str = detail["date_publish"]
                        if detail["body"] == None:
                            continue
                        if date_str:
                            date_publish = datetime.strptime(date_str,"%H:%M %d/%m/%Y")
                        else:
                            date_publish = None
                        Detail = DetailedPost(detail_post_id = randint(1,90),date_publish =date_publish
                        ,body = detail["body"],post = article)
                        Detail.save()




