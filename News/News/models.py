from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils.timezone import now

class Writer(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username


class Topic(models.Model):

    topic_id = models.CharField(max_length=255,default='topic_id',primary_key=True)
    name_topic = models.CharField(max_length=255)

    def __str__(self):
        return self.name_topic

    @property
    def to_dict(self):
        data = {
            'topic_id': self.topic_id,
            'name_topic': self.name_topic
        }
        return data




class Post(models.Model):

    article_id = models.IntegerField(primary_key=True)
    img_src = models.CharField(max_length=255)
    view_count = models.IntegerField(default = 0)
    valid = models.BooleanField(default = True)
    title = models.CharField(max_length=255, default = 'Title of some posts')
    summary = models.CharField(max_length=255,default='summary', null = True, blank = True)
    date = models.DateField('Date posted',default=now)
    link = models.CharField(max_length=50)
    body = models.TextField(default= "Body",null = True,blank= True)
    topic_id = models.ForeignKey(
        Topic,
        on_delete=models.SET_NULL,
        verbose_name="topic",
        blank = True,
        null = True
    )
    writers = models.ManyToManyField(User)

class Comment(models.Model):
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE
    )
    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE
    )
    comment = models.CharField(max_length = 50, default ="this is a comment")

class UserProfileInfo(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    portfolio_site = models.URLField(blank=True)
    profile_pic = models.ImageField(upload_to='profile_pics',blank=True)

    def __str__(self):
        return self.user.username


