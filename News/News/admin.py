from django.contrib.auth.models import Group
from django.contrib import admin
from django.contrib import auth
from django.contrib.admin.actions import delete_selected
from django.utils.html import format_html
#from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Topic,Post,Writer

class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ('title','topic_id','writers','view_count','img_src','date','time','link','summary','valid')}),
    ]
    list_display = ('title','get_topicname', 'date', 'view_count', 'get_writers', 'delete_button')
    search_fields = ['title']
    list_filter = ['date']

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return  self.readonly_fields + ('date','view_count','time')
        return self.readonly_fields
    readonly_fields = ('view_count',)
    """
    Display writers
    """
    def get_writers(self,obj):
        return ','.join([str(x) for x in obj.writers.all()])

    #delete actions delete
    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.username[0].upper() != 'J':
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions
    
    def get_topicname(self,obj):
        return obj.topic_id.name_topic


    #create delete button
    def delete_button(self, obj):
        str1 = '<a class="btn" href=http://localhost:8000/admin/News/post/'
        str2 = str(obj.id) + "/delete>Delete</a>"
        res = str1 + str2
        return format_html(res, obj.id)  
        
    delete_button.short_description = 'Delete'
    get_writers.short_description = 'Writers'
    get_topicname.short_description = 'Topic'
    
class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ('username',)}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email')}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser'),
        }),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    ]
    list_display = ('username','email', 'first_name', 'last_name', 'is_active', 'date_joined', 'is_staff','delete_button')
   
    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('last_login', 'date_joined')
        return self.readonly_fields
    readonly_fields = ('last_login',)
    #create delete button
    def delete_button(self, obj):
        str1 = '<a class="btn" href=http://localhost:8000/admin/auth/user/'
        str2 = str(obj.id) + "/delete>Delete</a>"
        res = str1 + str2
        return format_html(res, obj.id)  
    
        #delete actions delete
    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.username[0].upper() != 'J':
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    search_fields = ['username','email']
    list_filter = ['date_joined']
    delete_button.short_description = 'Delete'

class WriterAdmin(admin.ModelAdmin):
    model = Writer
    list_display = ('get_username','get_email','get_firstname','get_lastname')

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('user',)
        return self.readonly_fields

    def get_username(self, obj):
        return obj.user.username
    
    def get_email(self, obj):
        return obj.user.email
    
    def get_firstname(self, obj):
        return obj.user.first_name
    
    def get_lastname(self, obj):
        return obj.user.last_name
    
    def has_change_permission(self, request, obj=None):
        return False
    get_username.short_description = "Username"
    get_email.short_description = "Email"
    get_firstname.short_description = "First name"
    get_lastname.short_description = "Last name"

class TopicAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ('name_topic','topic_id')}),
    ]
    list_display = ('name_topic','topic_id')
    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('topic_id',)
        return self.readonly_fields
    readonly_fields = ('topic_id',)
    search_fields = ['name_topic']

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.unregister(Group)
admin.site.register(Topic,TopicAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Writer, WriterAdmin)