
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render,get_object_or_404,get_list_or_404,redirect
from .models import Topic,Writer,Comment
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .forms import RegisterUser,UserForm,UserFormFB,ChangePass,AddNews,CommentForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user, logout
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
# from registration.forms import RegistrationForm


import json
from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render,get_object_or_404,get_list_or_404
from .models import Topic,Post
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse

# def index(request):
#     form = RegisterUser()
#     list_topic = get_list_or_404(Topic)
#     username = "Đăng nhập"
#     # if  request.method == 'POST':
#     #     form = RegisterUser(request.POST)
#     #     if form.is_valid():
#     #         username = form.clean_username()
#     #         form.save()
#     #         return render(request, 'RegisSuccess.html',{'form':form,'list_topic':list_topic,'username':username})
#     #     else:
#     #         return render(request, 'register.html',{'form':form,'list_topic':list_topic,'username':username})
#     return render(request, 'index.html',{'form':form,'list_topic':list_topic,'username':username})


def index(request):

    list_topic = Topic.objects.all()
    list_trendy = Post.objects.all().order_by("-view_count")[:6]
    list_section = []
    username = "Đăng nhập"

    form = RegisterUser()
    for topic in list_topic:
        topic_id = topic.topic_id
        topic_name = topic.name_topic
        list_post_id = Post.objects.all().filter(topic_id=topic_id)
        if len(list_post_id) == 0:
            continue
        list_section.append(
            {
                topic_name: list_post_id
            }
        )

    if request.user.is_authenticated:
        username = request.user.username
        is_writer =False
        list_writer = Writer.objects.all()
        for i in list_writer:
            if i.user.username == username:
                is_writer = True
        return render(request,'Main_login.html',
                  {'list_topic':list_topic,'form':form,
                  'is_writer':is_writer,
                   'username':username,
                   'list_trendy':list_trendy,
                   'list_topic_article': list_section})

    return render(request,'index.html',
                  {'list_topic':list_topic,'form':form,
                   'username':username,
                   'list_trendy':list_trendy,
                   'list_topic_article': list_section})

def detail_post(request,article_id):

    post = Post.objects.all().filter(article_id = article_id)[0]
    can_comment = False
    forms = CommentForm()
    username = "Đăng nhập"
    logged = False
    is_writer = False
    if post:
        body = post.body
        list_comment = Comment.objects.filter(post_id=article_id).all()
        list_cmt = []
        for cmt in list_comment:
            cmt_item ={
                'user_name': User.objects.get(id = cmt.user_id).username,
                'comment': cmt.comment
            }
            list_cmt.append(cmt_item)
        if request.user.is_authenticated:
            can_comment = True
            username = request.user.username
            logged = True
            list_writer = Writer.objects.all()
            for i in list_writer:
                if i.user.username == username:
                    is_writer = True
            if request.method == 'POST':
                forms = CommentForm(request.POST)
                comment = forms.data['comment']
                forms.save(request.user,post,comment)

        return render(request,'detail_post.html',
                  {'detail_post':detail_post,
                  'is_writer':is_writer,
                  'logged':logged,
                   'post': post,
                   'body':body,
                   'username': username,
                   'list_comment':list_cmt,
                   'can_comment': can_comment,
                   'form': forms})



def articles_topic(request,topic_id):

    list_post = Post.objects.all().filter(topic_id=topic_id)
    username = "Đăng nhập"
    logged = False
    is_writer = False
    if request.user.is_authenticated:
        username = request.user.username
        logged = True
        list_writer = Writer.objects.all()
        for i in list_writer:
            if i.user.username == username:
                is_writer = True
    return render(request,'topic_article.html',{'list_post':list_post,'logged':logged,'is_writer':is_writer,
                   'username': username,})


def authen(request):
    return render(request,'./authen/authen.html')

def special(request):
    return HttpResponse("You are logged in !")

def register(request):
    form = RegisterUser()
    username = "Đăng nhập"
    if  request.method == 'POST':        
        form = RegisterUser(request.POST)
        if form.is_valid():
            username = form.clean_username()
            form.save()
            return render(request, 'RegisSuccess.html',{'form':form,'username':username})
        else:
            return render(request, 'register.html',{'form':form,'username':username})
    return render(request, 'index.html',{'form':form,'username':username})


def user_login(request):
    form = RegisterUser()
    list_topic = Topic.objects.all()[:8]
    post = get_list_or_404(Post)
    list_trendy = post[:6]
    list_section = []
    username = "Đăng nhập"
    form = RegisterUser()
    for topic in list_topic:
        topic_id = topic.topic_id
        topic_name = topic.name_topic
        list_post_id = Post.objects.all().filter(topic_id=topic_id)[:6]
        if len(list_post_id) == 0:
            continue
        list_section.append(
            {
                topic_name: list_post_id
            }
        )
    if  request.method == 'POST':
        list_topic = get_list_or_404(Topic)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        is_writer = False
        print(username, password)
        if user:
            if user.is_active:
                list_writer = Writer.objects.all()
                for i in list_writer:
                    if i.user.username == username:
                        is_writer = True
                login(request,user)
                return render(request, 'Main_login.html',{'list_topic':list_topic,'form':form,
                    'is_writer':is_writer,
                   'username':username,
                   'list_trendy':list_trendy,
                   'list_topic_article': list_section})
            else:
                username = "Đăng nhập"
                return render(request, 'login.html',{'formin':user,'form':form,'list_topic':list_topic,'form':form,
                   'username':username,
                   'list_trendy':list_trendy,
                   'list_topic_article': list_section})
        else:
            username = "Đăng nhập"
            return render(request, 'login.html',{'form':form,'formin':"Tài khoản hoặc mật khẩu không đúng",'list_topic':list_topic,'form':form,
                   'username':username,
                   'list_trendy':list_trendy,
                   'list_topic_article': list_section})

def addnews(request):

    form = AddNews()
    username = request.user.username
    if request.method == 'POST':
        form = AddNews(request.POST)
        title = form.data['title']
        topic_id = form.data['topic_id']
        summary = form.data['summary']
        body = form.data['body']
        date = form.data['date']
        form.save(title,topic_id,summary,body,date)
        return redirect('/')
    return render(request, 'addnews.html',{'form':form,
                   'username':username}
                 )

def user_logout(request):
    # print(request)
    logout(request)
    return HttpResponseRedirect('/')

def modify_user(request):
    list_topic = get_list_or_404(Topic)
    user = User.objects.get(username=request.user.username)
    check = True
    if request.method == 'POST':
        if user.social_auth.exists():
            user_form = UserFormFB(request.POST, instance=request.user)
            user_form.save()
            # messages.success(request, _('Your profile was successfully updated!'))
            return render(request, 'modifysuc.html',{'list_topic':list_topic})
        else:
            user_form = UserForm(request.POST, instance=request.user)
            # profile_form = ProfileForm(request.POST, instance=request.user.profile)
            if check_password(request.POST['password'], user.password):
                
                k = user_form.save(commit = False)
                k.password = user.password
                k.save()
                # messages.success(request, _('Your profile was successfully updated!'))
                return render(request, 'modifysuc.html',{'list_topic':list_topic})
            else:
                a = "Mật khẩu không đúng!"
                return render(request, 'modify_user.html', {
                        'test':a,
                        'user_form': user_form,
                        'list_topic':list_topic,
                    })

    else:
        user_form = UserForm(instance=request.user)
    if user.social_auth.exists():
        check = False
        user_form = UserFormFB(instance=request.user)
    return render(request, 'modify_user.html', {
        'check': check,
        'user_form': user_form,
        'list_topic':list_topic,
    })

def changepass(request):
    list_topic = get_list_or_404(Topic)
    user = User.objects.get(username=request.user.username)
    if request.method == 'POST':
        user_form = ChangePass(request.POST, instance=request.user)
        
        # profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if check_password(request.POST['password'], user.password):
            pass1 = request.POST['password1']
            pass2 = request.POST['password2']
            if pass1 == pass2:
                k = user_form.save(commit = False)
                user.set_password(pass2)
                k.password = user.password
                k.save()
                # messages.success(request, _('Your profile was successfully updated!'))
                return render(request, 'modifysuc.html',{'list_topic':list_topic})
            else:
                k = "Mật khẩu mới không trùng khớp!"
                return render(request, 'changepass.html', {
                    'test1':k,
                    'user_form': user_form,
                    'list_topic':list_topic,
                })
        else:
            a = "Mật khẩu không đúng!"
            return render(request, 'changepass.html', {
                    'test0':a,
                    'user_form': user_form,
                    'list_topic':list_topic,
                })

    else:
        user_form = ChangePass(instance=request.user)
    return render(request, 'changepass.html', {
        # 'username':request.user.username,
        'user_form': user_form,
        'list_topic':list_topic,
    })