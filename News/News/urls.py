"""News URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from . import views
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# from django.conf.urls.static import static
# from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name = 'index'),
    path('addnews',views.addnews,name = 'addnews'),
    path('l',views.user_login, name = 'user_login'),
    path('o',views.user_logout, name = 'user_logout'),
    path('changepassword',views.changepass, name = 'changepass'),
    path('modify',views.modify_user, name = 'modify_infor'),
    path('r',views.register, name = 'user_regis'),
    path('user_login',views.user_login,name='user_login'),
    path('post_detailed/<str:article_id>',views.detail_post,name ='detail_post'),
    path('topic/<str:topic_id>',views.articles_topic,name = 'topic articles'),
    path(r'^oauth/', include('social_django.urls', namespace='social')),
]

# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)