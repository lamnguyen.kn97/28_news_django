from django import forms
from .models import UserProfileInfo,Post,Topic,Comment
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import re
from random import randint
from django.contrib.auth.forms import UserCreationForm


class UserForm(forms.ModelForm):
    username = forms.CharField(label='Tên tài khoản', max_length=30,disabled =True)
    first_name = forms.CharField(label='Tên', max_length=30,)
    last_name = forms.CharField(label='Họ', max_length=30,)
    email = forms.CharField(label='Email')
    password = forms.CharField(label='Mật khẩu', widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','last_name','first_name','email','password')

class AddNews(forms.ModelForm):
    class Meta():
        model = Post
        fields = ('title','topic_id','summary','body','date')

    def save(self,title,topic_id,summary,body,date):
        tp = Topic.objects.get(topic_id=topic_id)
        article_id = randint(1000,9999)
        post = Post(topic_id = tp,summary = summary,title = title,
                    body = body,date = date,article_id = article_id)
        post.save()

class CommentForm(forms.ModelForm):
    class Meta():
        model = Comment
        fields = ('comment',)
    comment = forms.CharField(label='Comment', max_length = 50)
    def save(self,user,post,comment):
        cmt = Comment(user_id=user.id,post_id=post.article_id, comment=comment)
        cmt.save()


    

class UserFormFB(forms.ModelForm):
    username = forms.CharField(label='Tên tài khoản', max_length=30,disabled =True)
    first_name = forms.CharField(label='Tên', max_length=30,)
    last_name = forms.CharField(label='Họ', max_length=30,)
    email = forms.CharField(label='Email')
    class Meta():
        model = User
        fields = ('username','last_name','first_name','email')

class ChangePass(forms.ModelForm):
    password = forms.CharField(label='Mật khẩu cũ', widget=forms.PasswordInput())
    password1 = forms.CharField(label='Mật khẩu mới', widget=forms.PasswordInput())
    password2 = forms.CharField(label='Nhập lại mật khẩu mới', widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('password','password1','password2')


class RegisterUser(UserCreationForm):

    username = forms.CharField(label='Tên tài khoản', max_length=30)
    email = forms.CharField(label='Email')
    password1 = forms.CharField(label='Mật khẩu', widget=forms.PasswordInput())
    password2 = forms.CharField(label='Nhập lại mật khẩu', widget=forms.PasswordInput())

    def clean_password(self):
        print(self.cleaned_data)
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data.get('password1', None)
            password2 = self.cleaned_data.get('password2')
            # print(password, passwordre)
            if password1 == password2 and password1 is not None and password2 is not None:
                return password2
        raise forms.ValidationError("Mật khẩu không hợp lệ")

    def clean_username(self):
        username = self.cleaned_data['username']
        print("ues", username)
        # if re.search(r'^w+$',username):
        #     raise forms.ValidationError("Tên tài khoản chứa ký tự đặc biệt")

        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        # teen trung
        raise forms.ValidationError("Tên tài khoản đã tồn tại")

    def save(self):
        User.objects.create_user(username=self.cleaned_data['username'], email=self.cleaned_data['email'],
                                 password=self.cleaned_data['password1'])
