import scrapy
import json
import numpy as np
from random import randint
from random import seed
import datetime
from crawl_news.items import PostItem
from News.News.models import Post
from News.News.models import Topic

seed(99)

def convert_to_article_item(zing, article,trendy,topic_name = "Trendy topic"):
    article_item = PostItem()
    date_string = article.css('span.article-publish').css('span.date::text').get() if zing else "20/5/2019"
    date = datetime.datetime.strptime(date_string, "%d/%m/%Y")

    id = randint(10,9999)
    article_obj = Post.objects.filter(article_id = id)
    if article_obj and zing:
        return
    if trendy:
        article_item['view_count'] = 99
        dt_src = 'src'
    else:
        dt_src='data-src'
        if topic_name == 'TTDN':
            topic_name = 'Fsell'
        elif topic_name.strip() == 'Du lịch & Ẩm thực':
            topic_name = 'Du lịch'
        elif topic_name == 'Số hóa':
            topic_name = "Công nghệ"
        elif topic_name == 'Đời sống':
            topic_name = 'Nhịp sống'
        elif topic_name == 'Xe':
            topic_name = 'Xe 360'
        topic = Topic.objects.filter(name_topic=topic_name).all()
        if topic:
            article_item['topic_id'] = topic[0]

    if not zing:
        print(topic_name)
    article_item['article_id'] = id
    article_item['img_src'] = article.css('p.article-thumbnail').css('a').css('img').attrib[dt_src] if zing else \
        article.css('a.thumb img').attrib['src']
    article_item['title'] = article.css('p.article-title').css('a::text').get() if zing else \
        article.css('a.thumb').attrib['title'] if trendy else article.css('h4.title_news a::text').get()
    article_item['summary'] = article.css('p.article-summary::text').get() if zing else \
        article.css('p.description::text').get()
    article_item['link'] = f'/post_detailed/{id}'
    article_item['date'] = date

    print(article_item)
    return article_item

class ArticleSpider(scrapy.Spider):
    name = "article"

    def start_requests(self):
        urls = [
            'https://news.zing.vn/',
            'https://vnexpress.net/'
        ]
        for url in urls:

            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url
        if page == 'https://news.zing.vn/':
            trendy_section= response.css('section#section-featured')
            list_trendy = trendy_section.css('div.article-list')
            for ls in list_trendy:
                list_article = ls.css('article.article-item')
                for article in list_article:
                    article_item = convert_to_article_item(True,article,trendy=True)
                    if article_item is None:
                        continue
                    link = article.css('p.article-thumbnail').css('a').attrib['href']
                    if link is not None:
                        next_page = response.urljoin(link)
                        yield scrapy.Request(next_page, callback=self.parse_detail, meta={'article_item': article_item})


            section_content = response.css('section.section-content').css('div.box-category')
            for category in section_content:
                title = category.css('h3.category-title').css('a::text').get()
                print(title)
                list_art = category.css('div.article-list').css('article.article-item')
                for art in list_art:
                    article_item = convert_to_article_item(True,art,trendy= False, topic_name= title)
                    if article_item is None:
                        continue
                    link = art.css('p.article-thumbnail').css('a').attrib['href']
                    if link is not None:
                        next_page = response.urljoin(link)
                        yield scrapy.Request(next_page, callback=self.parse_detail,meta={'article_item':article_item})

        else:
            trendy_section = response.css('section.featured.container')
            article = trendy_section.css('article')
            article_item = convert_to_article_item(False,article,trendy = True)
            link = trendy_section.css('a.thumb').attrib['href']
            if link is not None:
                next_page = response.urljoin(link)
                yield scrapy.Request(next_page, callback=self.parse_detail, meta={'article_item': article_item})
            list_category = response.css('section.box_category')
            for category in list_category:
                article_list = category.css('article.list_news')
                topic = list_category.css('h2').css('a.first').attrib['title']
                print(topic)
                for article in article_list:
                    article_item = convert_to_article_item(False,article,False,topic)
                    if article_item is None:
                        continue
                    link = trendy_section.css('a.thumb').attrib['href']
                    if link is not None:
                        next_page = response.urljoin(link)
                        yield scrapy.Request(next_page, callback=self.parse_detail, meta={'article_item': article_item})

    def parse_detail(self, response):
        page = response.url
        article_item = response.meta['article_item']
        if 'https://news.zing.vn/' in page:
            related = response.css('section.main').css('section.sidebar').get()
            body = response.css('section.main').get().replace(related, '')
            article_item['body'] = body
            yield article_item

        else:
            body = []
            section = response.css('section.sidebar_1').get()
            # body += section.css('header').get()
            # body += section.css('h1.title_news_detail').get()
            # body += section.css('p.description').get()
            # body += section.css('article.content_detail').get()
            article_item['body'] = section
            yield article_item














