import scrapy
import json
from random import randint
from crawl_news.items import TopicItem
from News.News.models import Topic
class TopicSpider(scrapy.Spider):
    name = "topic"

    def start_requests(self):
        urls = [
            'https://news.zing.vn/',
            'https://vnexpress.net/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url
        if page == 'https://news.zing.vn/':
            nav = response.css('nav.category-menu')
            list_topic = nav.css('li.parent')
            for tp in list_topic:
                name_topic = tp.css('a::text').get()
                topic = Topic.objects.filter(name_topic = name_topic)
                if topic:
                    continue
                tp_item = TopicItem()
                tp_item["topic_id"] = randint(10,999)
                tp_item["name_topic"] = name_topic
                yield tp_item

        else:
            nav = response.css('nav.p_menu')
            list_topic = nav.css('a')
            for tp in list_topic:
                name_topic = tp.css('::text').get()
                if name_topic == None:
                    continue
                name_topic = name_topic.strip()
                if name_topic == 'Số hóa':
                    name_topic = "Công nghệ"
                elif name_topic == 'Đời sống':
                    name_topic = 'Nhịp sống'
                elif name_topic == 'Xe':
                    name_topic = 'Xe 360'
                topic = Topic.objects.filter(name_topic = name_topic)
                if topic:
                    continue
                tp_item = TopicItem()
                tp_item["topic_id"] = randint(10, 999)
                tp_item["name_topic"] = name_topic
                yield tp_item








