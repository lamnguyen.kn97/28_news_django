# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from News.News.models import Topic,Post
from scrapy_djangoitem import DjangoItem

class TopicItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    django_model = Topic

class PostItem(DjangoItem):

    django_model = Post

